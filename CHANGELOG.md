## 0.2.2 (2022-06-26)

Build updates

## 0.2.1 (2022-06-26)

Build updates

## 0.2.0 (2022-06-26)

### Feat

- asset name mappings

## 0.1.1 (2022-04-13)

### Fix

- remove print

## 0.1.0 (2022-01-30)

Build updates
## 0.0.2 (2022-01-29)

### Feat

- complete basic get_'s
- complete requests migration
- move towards requests [wip]

## 0.0.1 (2022-01-25)

### Feat

- add asteroid retrieval [wip]
